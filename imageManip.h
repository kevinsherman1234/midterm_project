// Daniel Stambler, Kevin Sherman
// 601.220
// 10/15/2017
// Midterm Project
// dstambl2 ksherma6
// dstambl2@jhu.edu ksherma6@jhu.edu

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

 #define MAX_COLOR_VAL 255
 #define MIN_COLOR_VAL 0


void brightness(Image *image, int delta);

void swap(Image* x);

void grayscale(Image* im);

void crop(Image *image, int x1, int y1, int x2, int y2);

void contrast(Image *image, double amt);

Image* blur(Image *image, double sigma);

void sharpen(Image *image, double sigma, double amt);

void edges(Image *image, double sigma, double threshold);


#endif
