// Daniel Stambler, Kevin Sherman
// 601.220
// 10/15/2017
// Midterm Project
// dstambl2 ksherma6
// dstambl2@jhu.edu ksherma6@jhu.edu

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "menuUtil.h"
#include "ppmIO.h"
#include "imageManip.h"

/**
 prints menu
 **/
void printMenu() {
  printf("Main menu: \n");
  printf("\t r <filename> - read image from <filename>\n");
  printf("\t w <filename> - write image to <filename>\n");
  printf("\t s - swap color channels\n");
  printf("\t br <amt> - change brightness (up or down) by the given amount\n");
  printf("\t c <x1> <y1> <x2> <y2> - crop image to the box with given corners\n");
  printf("\t g - convert to grayscale\n");
  printf("\t cn <amt> - change contrast (up or down) by given amount\n");
  printf("\t bl <sigma> - Gaussian blur with the given radius (sigma)\n");
  printf("\t sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n");
  printf("\t e <sigma> <threshold> - detect edges with intensity gradient greater than the given threshold\n");
  printf("\t q - quit\n");

  printf("Enter choice: ");

}


/**
 This will be the function main calls, all menu stuff will take place here
 and all
 **/
void menuUt() {

  char input[30];
  char filename[256];
  char str[256];
  Image* ppmImg = NULL;
  printMenu();

  //loops through menu until q is hit
  while (scanf("%s",input)) {

    //READS image
    if (strcmp(input, "r") == 0){
      scanf("%s", filename);
      printf("%s\n", filename);
      if(ppmImg) {
	if(ppmImg->data){
	  free(ppmImg->data);
	}
	free(ppmImg);
      }
      ppmImg = readPPM(filename);
      if (ppmImg != NULL) {
	printf("Read Successful\n");
      }
      fgets(str, sizeof(str), stdin);
     }

    //Writes image
    else if (strcmp(input, "w") == 0) {
       scanf("%s", filename);

       if (ppmImg == NULL) {
	 printf("No Image\n");
       }
       else {
          //Writes to file
          FILE *fp = fopen(filename, "wb");
          // write(pix, rows, cols, 255, fp); //VARS NEED TO BE DECLARED
          writeImage(ppmImg, fp);
          fclose(fp);
          printf("File written %s\n", filename);
       }
       fgets(str, sizeof(str), stdin);
     }

    //brightness
    else if (strcmp(input, "br") == 0) {
       int delta = 0;
       //ensures delta is int
       if (scanf("%d", &delta) == 1) {
          if (ppmImg == NULL || ppmImg->data == NULL){
            printf("invalid image\n\n");
          }
	  else {
             if (delta > MAX_COLOR_VAL || delta < -(MAX_COLOR_VAL)){
             printf("Please enter a value between -255 and 255\n");
             } else
              brightness(ppmImg, delta);
	     printf("brightness successful\n");
          }
       }
       else {
	 printf("Brightness input not an int\n");
	 fgets(str, sizeof(str), stdin);
       }

     }

    //SWAP
    else if (strcmp(input, "s") == 0) {
       if (ppmImg == NULL) {
	 printf("NO IMAGE \n");
       }
       else {
	 swap(ppmImg);
	 printf("Swap Successful \n");
       }
       fgets(str, sizeof(str), stdin);
     }

    //Converts to GrayScale
    else if (strcmp(input, "g") == 0) {
       if (ppmImg == NULL) {
	 printf("NO IMAGE \n");
       }
       else {
         grayscale(ppmImg);
	 printf("Gray complete\n");
       }
       fgets(str, sizeof(str), stdin);
     }

     //crop
     else if(strcmp(input, "c") == 0) {
       int x1, x2, y1, y2;
       if(scanf("%d %d %d %d", &x1, &y1, &x2, &y2) == 4) {
	 if (ppmImg == NULL) {
	   printf("NO IMAGE \n");
	 }
	 else if(x1 <= x2 && y1 <= y2 && x1 >= 0 && x2 < (int)ppmImg->cols
		 && y1 >= 0 && y2 < (int)ppmImg->rows) {
	   crop(ppmImg, x1, y1, x2, y2);
	   printf("Crop Successful \n");
	 } else {
	   printf("Invalid input \n");
	 }
       }
       else {
	 printf("Invalid input \n");
	 fgets(str, sizeof(str), stdin);
       }
     }

     //contrast
     else if (strcmp(input, "cn") == 0) {
        double amt = 0;
        if (scanf("%lf", &amt) == 1) {
           if (ppmImg == NULL || ppmImg->data == NULL){
             printf("invalid image\n\n");
           } else {
             contrast(ppmImg, amt);
             printf("contrast Successful \n");
           }
	}
	else {
	  printf("Input must be a double\n");
	  fgets(str, sizeof(str), stdin);
	}

      }


    //blur
     else if (strcmp(input, "bl") == 0) {
       double sigma = 0;
       //checks for correct input
       if (scanf("%lf", &sigma) == 1) {
         if (ppmImg == NULL) {
           printf("invalid image\n");
         } else {
	   ppmImg = blur(ppmImg,sigma);
	   printf("Blur Successful \n");
         }
       }
       else {
	 printf("Input must be a double \n");
	 fgets(str, sizeof(str), stdin);
       }
     }

    //Sharpen
     else if (strcmp(input, "sh") == 0) {
       double sigma = 0;
       double amt = 0;
       if(scanf("%lf %lf", &sigma, &amt) == 2) {
	 if (ppmImg == NULL) {
	   printf("invalid image\n");
	 } else {
	   sharpen(ppmImg, sigma, amt);
	   printf("Sharpen Successful \n");
	 }
       }
       else {
         printf("both inputs must be doubles\n");
	 fgets(str, sizeof(str), stdin);
       }
     }

    //edge detection
     else if (strcmp(input, "e") == 0) {
       double sigma = 0;
       double threshold = 0;
       if (scanf("%lf %lf", &sigma, &threshold) == 2) {
         if (ppmImg == NULL) {
	   printf("NO IMAGE \n");
         }
         else {
           edges(ppmImg, sigma, threshold);
	   printf("edge detection complete\n");
         }
       }
       else {
         printf("both inputs must be doubles\n");
	 fgets(str, sizeof(str), stdin);
       }
     }

    //quits program
     else if (strcmp(input, "q") == 0) {
       break;
     }


    //stops invalid input
     else {
       printf("invalid input \n");
       fgets(str, sizeof(str), stdin);
     }

      printMenu();
  }

  if(ppmImg){
    if(ppmImg->data){
      free(ppmImg->data);
    }
    free(ppmImg);
  }
}
