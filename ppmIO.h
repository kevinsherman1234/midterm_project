// Daniel Stambler, Kevin Sherman
// 601.220
// 10/15/2017
// Midterm Project
// dstambl2 ksherma6
// dstambl2@jhu.edu ksherma6@jhu.edu

#ifndef PPMIO_h
#define PPMIO_h

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct _pixel {
  unsigned char red;
  unsigned char green;
  unsigned char blue;
} Pixel;

typedef struct _image {
  unsigned int cols;
  unsigned int rows;
  unsigned char colors;
  Pixel *data;
} Image;

Image* readPPM(char* fileName);
int write(Pixel *image, int rows, int cols, int colors, FILE *fp);
int writeImage(Image *im, FILE *fp);

#endif
