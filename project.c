// Daniel Stambler, Kevin Sherman
// 601.220
// 10/15/2017
// Midterm Project
// dstambl2 ksherma6
// dstambl2@jhu.edu ksherma6@jhu.edu

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "menuUtil.h"
#include "ppmIO.h"
#include "imageManip.h"

int main() {
  //This will be the call to a function in menu util.
   menuUt();

  return 0;
}
