// Daniel Stambler, Kevin Sherman
// 601.220
// 10/15/2017
// Midterm Project
// dstambl2 ksherma6
// dstambl2@jhu.edu ksherma6@jhu.edu

//Contains implementations of all image manipulation algorithms
#include "ppmIO.h"
#include "imageManip.h"
#include <math.h>

#define PI 3.14159265358979323846

/**
 Checks bounds on brightness
 **/
unsigned char saturation_add(unsigned char color, int delta){
  int i = color + delta;
  if (i > MAX_COLOR_VAL){
    return MAX_COLOR_VAL;
  }
  if (i < MIN_COLOR_VAL){
    return MIN_COLOR_VAL;
  }
  return i;
}

/**
 Does brightness
 **/
void brightness(Image *image, int delta){

  Pixel *pxl = image->data;
  int j = image->cols * image->rows;
  for(int i = 0; i < j; i++) {
    pxl->red = saturation_add(pxl->red, delta);
    pxl->green = saturation_add(pxl->green, delta);
    pxl->blue = saturation_add(pxl->blue, delta);
    pxl++;
  }
}

/**
Swap function
 **/
void swap(Image *x) {
  int size = x->rows * x->cols;
  for (int i = 0; i < size; i++) {
       //Swap function
       Pixel swappy = x->data[i];
       unsigned char temp =  swappy.red;
       swappy.red = swappy.green;
       swappy.green = swappy.blue;
       swappy.blue = temp;
       x->data[i] = swappy;
   }
}

/**
 Turns the image gray
 **/
void grayscale(Image *im) {
  int size = im->rows * im->cols;
  for (int i = 0; i < size; i++) {
    Pixel greyer = im->data[i];

    //Convert Char to Int
    int red = greyer.red;
    int blue = greyer.blue;
    int green = greyer.green;

    //Adds them together
    int greyintense = (0.30 * red) + (0.59 * green) + (0.11 * blue);

    //Extra check to ensure nothing goes over
    if (greyintense > 255 ) {
      greyintense = 255;
    }

    unsigned char grey = greyintense;

    //Assigns Each Pixel a new value.
    greyer.red = grey;
    greyer.blue = grey;
    greyer.green = grey;

    //Derefrences
    im->data[i] = greyer;
  }
}

/**
Crops the image
 **/
void crop(Image *image, int x1, int y1, int x2, int y2){
  int new_rows = (y2 - y1 + 1);
  int new_cols = (x2 - x1 + 1);
  int i = 0;
  Pixel *new_data = malloc(sizeof(Pixel) * new_cols * new_rows);
  for(int y = y1; y <= y2; y++){
    for (int x = x1; x <= x2; x++){
      new_data[i] = image->data[image->cols*y+x];
      i++;
    }
  }
  free(image->data);
  image->data = new_data;
  image->rows = new_rows;
  image->cols = new_cols;
}

/**
 Checks colors bounds for contrast
 **/

unsigned char contrast_adj(unsigned char val, double amt){
  double shift = (MAX_COLOR_VAL + MIN_COLOR_VAL) / 2.0;
  double value = ((double) val - shift) * amt + shift;
  if (value > MAX_COLOR_VAL){
    return MAX_COLOR_VAL;
  }
  if (value < MIN_COLOR_VAL){
    return MIN_COLOR_VAL;
  }
  return value;
}

/**
 Does Contrast
 **/
void contrast(Image *image, double amt){
  int size = image->rows * image->cols;
  for (int i = 0; i < size; i++) {
    image->data[i].red = contrast_adj(image->data[i].red, amt);
    image->data[i].blue = contrast_adj(image->data[i].blue, amt);
    image->data[i].green = contrast_adj(image->data[i].green, amt);
  }
}

//THE FOLLOWING IS FOR BLURRING THE IMAGE

/**
Squares a value. For the blur function.
 **/
double sq(double n) {
  double square = n * n;
  return square;
}

/**
Generates Blur Matrix
 **/
float** blurMatrix(double sigma, int* sz) {
  int delta = (int) (sigma * 5.0);
  int size = delta * 2 + 1;
  
  float **g = (float**) malloc( sizeof(float*) * size);
  
  for (int dx = -delta; dx <= delta; dx++) {
    g[dx + delta] = (float*)malloc(sizeof(float) * size);
    
    for (int dy = -delta; dy <= delta; dy++) {
       g[dx + delta][dy + delta] = (1.0/ (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2.0 * sq(sigma)));
      }
  }
  *sz = size;
  return g; 
}


/**
Ensures the bounds are kept
**/
unsigned char bound_pixel(float val) {
  if (val < 0) {
    return 0;
  }
  else if (val > 255) {
    return 255;
  }
  else {
    return (unsigned char) val;
  }
}


/**
Applies blur to the specific pixel
**/
Pixel convolve(float** matrix, int x, int y, Image* im, int mat_size)
{
  Pixel pix;
  float red_total = 0.0;
  float green_total = 0.0;
  float blue_total = 0.0;
  int delta = mat_size / 2;
  float total = 0;
  
  for (int i = 0; i < mat_size; i++){
    for (int j= 0; j < mat_size; j++){

      //does the operation
      int xlocation = x + i - delta;
      int ylocation = y + j - delta;
      int location = xlocation + im->cols * ylocation;
        
      //insure location is in the bounds of data array
      if ((xlocation >= 0) && (xlocation < (int)im->cols) &&
	  (ylocation >=0) && (ylocation < (int)im->rows)) {
          red_total += im->data[location].red * matrix[i][j];
          green_total += im->data[location].green * matrix[i][j];
          blue_total += im->data[location].blue * matrix[i][j];
	  total += matrix[i][j];
	}
    }
  }

  //Sets the color channels of pix
  pix.red =  bound_pixel(red_total / total);
  pix.green = bound_pixel(green_total / total);
  pix.blue = bound_pixel(blue_total / total);

  return pix;
}



/**
Blurs the image.
Calls the matrix function. Then loops through all pixels
and calls filter response function. 
**/

Image* blur(Image *image, double sigma) {
  int mat_size;
  float** mat = blurMatrix(sigma, &mat_size);

  Pixel *new_data = malloc(sizeof(Pixel) * image->cols * image->rows);
  
   for (int x = 0; (unsigned int) x < image->cols; x++) {
     for (int y = 0; (unsigned int) y < image->rows; y++) {
       int location = x + image->cols * y;

       new_data[location] = convolve(mat, x, y, image, mat_size);
      }
    }

   //frees previously allocated memory
   free(image->data);
   image->data = new_data;

   for (int i = 0; i < mat_size; i++) {
     free(mat[i]);
   }
   
   free(mat);
   return image;
  }

//The rest of the file now deals with sharpen and edge detection

/**
The sharpen function.
Calls the blur function
**/
void sharpen(Image* image, double sigma, double amt) {
  //Gets a blurred image

  Pixel* savedData = (Pixel*) malloc(sizeof(Pixel) * image->rows * image->cols);
  for (unsigned int i = 0; i < image->rows * image->cols; i++) {
    savedData[i] = image->data[i];
  }
  
  Image* blured = blur(image, sigma);

  int size = image->rows * image->cols;

  for (int i = 0; i < size; i++) {
       Pixel normal = savedData[i];
       Pixel pixel_blured = blured->data[i];
       
       int red_norm =  normal.red;
       int blue_norm = normal.blue;
       int green_norm = normal.green;

       int red_blur = pixel_blured.red;
       int blue_blur = pixel_blured.blue;
       int green_blur = pixel_blured.green;

       //Sharpens image
       float new_red = red_norm + ((red_norm - red_blur) * amt);
       float new_blue = blue_norm + ((blue_norm - blue_blur) * amt); 
       float new_green = green_norm + ((green_norm - green_blur) * amt);


       //Converts int to char
       unsigned char final_red = bound_pixel(new_red);
       unsigned char final_blue = bound_pixel(new_blue);
       unsigned char final_green = bound_pixel(new_green);

       //Assigns new sharpen value
       normal.red = final_red;
       normal.blue = final_blue;
       normal.green = final_green;
	 
       image->data[i] = normal;
  }
  free(savedData);
}

/**
Does Edge Detection
 **/
void edges(Image *image, double sigma, double threshold) {
  grayscale(image);
  blur(image, sigma);
  Pixel (*pxls)[image->cols] = (Pixel (*)[image->cols])image->data;
  Pixel (*new_data)[image->cols] =
    malloc(sizeof(Pixel) * image->cols * image->rows);
  memset(new_data, 0, sizeof(Pixel) * image->cols * image->rows);
  for(unsigned int y = 1; y < image->cols - 1; y++){
    for(unsigned int x = 1; x < image->rows - 1; x++){
      double Gx = ((pxls[x+1][y].red) - (pxls[x-1][y].red)) / 2.0;
      double Gy = ((pxls[x][y+1].red) - (pxls[x][y-1].red)) / 2.0;
      double mG = sqrt(sq(Gx) + sq(Gy));
      if(mG > threshold){
        new_data[x][y].red = 0;
        new_data[x][y].green = 0;
        new_data[x][y].blue = 0;
      } else {
        new_data[x][y].red = 255;
        new_data[x][y].green = 255;
        new_data[x][y].blue = 255;
      }

    }
  }
  free(image->data);
  image->data = (Pixel*)new_data;
}
