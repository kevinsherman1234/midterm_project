# Daniel Stambler, Kevin Sherman
# 601.220
# 10/15/2017
# Midterm Project
# dstambl2 ksherma6
# dstambl2@jhu.edu ksherma6@jhu.edu

# Makefile of HW4

CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g

# Links together files needed to create executable
project: project.o menuUtil.o ppmIO.o imageManip.o
	$(CC) -o main project.o menuUtil.o ppmIO.o imageManip.o -lm

# Compiles project.c to create project.o
# Note that we list functions.h here as a file that
# main.c depends on, since main.c #includes it
project.o: project.c menuUtil.h ppmIO.h imageManip.h
	$(CC) $(CFLAGS) -c project.c

# Compiles menuUtil.c to create menuUtil.o
# Note that we list functions.h here as a file that
# interface.c depends on, since interface.c #includes it
menuUtil.o: menuUtil.c menuUtil.h ppmIO.h imageManip.h
	$(CC) $(CFLAGS) -c menuUtil.c

# Compiles ppmIO.c to create ppmIO.o
# Note that we list functions.h here as a file that
# interface.c depends on, since interface.c #includes it
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c

# Compiles imageManip.c to create imageManip.o
# Note that we list functions.h here as a file that
# interface.c depends on, since interface.c #includes it
imageManip.o: imageManip.c imageManip.h ppmIO.h
	$(CC) $(CFLAGS) -c imageManip.c

# Removes all object files and the executable named project,
# so we can start fresh
clean:
	rm -f *.o project
