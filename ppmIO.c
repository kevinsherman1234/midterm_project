// Daniel Stambler, Kevin Sherman
// 601.220
// 10/15/2017
// Midterm Project
// dstambl2 ksherma6
// dstambl2@jhu.edu ksherma6@jhu.edu

//Implements functions for reading, writing, creating, destroying, copying, etc

#include <stdio.h>
#include <errno.h>
#include "ppmIO.h"
#define MAX_BUFFER_SIZE 256
#define COMMENT "#CREATOR: Basic image manipulator by Daniel Stambler & Kevin Sherman Version 0.1"
 
/**
 Writes the image.
 Code provided from Prof's example
 **/
int write(Pixel *image, int rows, int cols, int colors, FILE *fp) { 
    if (!fp) {
      fprintf(stderr, "ERROER Bad Filename\n");
      return 0;
    }
    //Tag
    fprintf(fp, "P6\n");

    //dimesnions
    fprintf(fp, "%s\n%d %d\n%d\n", COMMENT, cols, rows, colors);
    
    //Pixels
    int writes = fwrite(image, sizeof(Pixel), rows * cols, fp);
    if (writes != (rows * cols)) {
      fprintf(stderr, "Error: Failed to write data to file!\n");
    }
    return writes;
 }

/**
Saves image to exact file
 **/
int writeImage(Image *im, FILE *fp) {
  return write(im->data, im->rows, im->cols, im->colors, fp);
}


/**
Reads an image
 **/
Image* readPPM(char* fileName){
  FILE *fp = fopen(fileName, "rb");
  if(fp == NULL) {
    printf("Failed to open %s.%s\n", fileName, strerror(errno));
    return NULL;
  }
  char buf[MAX_BUFFER_SIZE];
  char c;
  fgets(buf, sizeof(buf), fp);
  if (strncmp(buf, "P6", 2)) {
    printf("%s is an illegal ppm format\n", fileName);
    fclose(fp);
    return NULL;
  }

  Image* ppmImg = malloc(sizeof(Image));
  while ((c=fgetc(fp)) == '#'){
    fgets(buf, sizeof(buf), fp);
  }
  ungetc(c, fp);

  fscanf(fp, "%ud", &(ppmImg->cols));

  while ((c=fgetc(fp)) == '#'){
    fgets(buf, sizeof(buf), fp);
  }
  ungetc(c, fp);

  fscanf(fp, "%ud", &(ppmImg->rows));

  while ((c=fgetc(fp)) == '#'){
    fgets(buf, sizeof(buf), fp);
  }
  ungetc(c, fp);
  int color_temp;
  fscanf(fp, "%d", &color_temp);
  if(color_temp > 255) {
    printf("%s has invalid colors: %d", fileName, color_temp);
    free(ppmImg);
    fclose(fp);
    return NULL;
  }
  ppmImg->colors = color_temp;

  ppmImg->data = malloc(sizeof(struct _pixel) * ppmImg->cols * ppmImg->rows);
  //skip the last white space before the pixel representation
  fgetc(fp);

  //number of colors is 3
  fread(ppmImg->data, sizeof(unsigned char), ppmImg->cols * ppmImg->rows * 3, fp);
  fclose(fp);
  return ppmImg;


}
